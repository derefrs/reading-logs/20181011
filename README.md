# 20181011 - Deref.rs (test)

Thu, 11. Oct. 2018

https://derefrs.connpass.com/

```toml
[meetup]
name = "Deref Rust"
date = "20181011"
version = "0.0.0"
attendants = [
  "Yasuhiro Asaka <yasuhiro.asaka@grauwoelfchen.net>"
]
repository = "https://gitlab.com/derefrs/reading-logs/20181011"
keywords = ["Rust"]

[locations]
park6 = {site = "Roppongi"}
zulip = {site = "https://derefrs.zulipchat.com", optional = true }
```

## Notes

| Name | Snippet |
|--|--|
| @grauwoelfchen | https://gitlab.com/derefrs/reading-logs/20181011/snippets/1761984 |
|||


## Links

* [Deref Rust - GitLab.com](https://gitlab.com/derefrs)
* [Deref Rust - Zulip](https://derefrs.zulipchat.com/)
* [Deref Rust - Telepost](https://derefrs.telepost.blog/) (ja)
